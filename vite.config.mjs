/// <reference types="vitest" />

import react from "@vitejs/plugin-react";
import { defineConfig } from "vite";

const config = defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    environment: "jsdom",
    reporters: ["hanging-process", "default"],
    exclude: ["_template", "node_modules", "dist", ".git", ".cache"],
    setupFiles: "./vitest.setup.ts",
    environmentOptions: {
      jsdom: {
        console: true,
      },
    },
    sequence: {
      hooks: "list",
    },
  },
  clearScreen: false,
});

export default config;
