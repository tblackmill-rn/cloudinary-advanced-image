import { Cloudinary } from "@cloudinary/url-gen";
import {
  AdvancedImage,
  lazyload,
} from "@cloudinary/react";
import { FC } from "react";

const imageSource = new Cloudinary({ cloud: { cloudName: "demo" } });
const image = imageSource.image("blue-water");

const SmartImage: FC = () => {
  return (
    <AdvancedImage
      cldImg={image}
      style={{ maxWidth: "100%" }}
      plugins={[
        lazyload(),
      ]}
    />
  );
}

export default SmartImage;