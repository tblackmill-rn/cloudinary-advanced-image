import { render, screen } from "@testing-library/react";
import SmartImage from "./SmartImage";

export const smartImageDelay = async (ms: number = 50) => {
    return new Promise((resolve) => 
        setTimeout(resolve, ms));
};

describe(`SmartImage`, () => {
  it("shows an image", async () => {
    render(<SmartImage />);

    const subject = screen.getByRole("img");
    await smartImageDelay()

    expect(subject).toBeInTheDocument();
    expect(subject).toHaveAttribute("src", expect.stringMatching("blue-water"))
  });
});
